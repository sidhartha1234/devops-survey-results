If people marked that they were concerned about an issue that
wasn't listed, they were asked to describe the concern in a free-form text box. While you could go look at the csv in this
directory to find out what those concerns were, I figured I'd
do your eyes a favor and put it here a little more nicely formatted.

Note that some minor punctuation changes were made from the
exact responses people entered, to make the csv a little
easier to parse (commas were replaced with semicolons or
periods).

I'll also put a numerical indicator to show how concerned the
respondent was about the issue. 0 corresponds to "pretty much
impossible," and 3 corresponds to "constant worry"

- 3: All of the above
- 3: Deployments are too slow due to application layer issues - poor code; etc..
- 3: Every time our CTO gets up I worry about what fresh hell he's about to drop on the company.
- 2: Finding qualified people!
- 3: I don't like the kind of work I do. Spending all day looking at logs; not doing any real engineering
- 3: I don't think we have enough power to effect the above categories; and I wouldn't want anything more than an advisory role
- 1: Lack of available metrics and logging in our apps; which makes it quite difficult to troubleshoot and resolve issues.
- 3: Not enough time to fix looming technical debt
- 2: Not shipping new features fast enough
- 2: Notre muchas care from tram and organisation for Ci/CD or its principles
- 2: Remote working & the troubles of being informed properly from involved parties
- 0: Scalability
- 2: Struggling between teams and problems that are way out of my hands.
- 3: We aren't spending time effectively where business value should be generated
- 3: Will there be customer facing application level issues that cause revenue loss that are undetectable with the current monitoring and metrics.
